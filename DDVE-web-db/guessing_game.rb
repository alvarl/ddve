# Sorry again to Didzis and Himshwet!
# This is a sample solution to a small programming assignment left to others this afternoon
# J2tsin t2pit2hed 2ra, et k6ik otse kopipasteda saaks selle

vastus = ''
# Maarame oma pakkumise miinimumi ja maksimumi
min = 1
max = 100

puts 'Motle valja arv 1-100 ja vajuta <ENTER>'
gets

while(vastus != 'y') # kui meie vastus oli OK, lopetame pakkumise
  pakkumine = (max + min) / 2 # leiame miinimumi ja maksimumi vahel keskmise pakkumise
  puts "Kas see on #{pakkumine} ? (vastuseks suurem (s), vaiksem (v) voi oige (y)"
  vastus = gets.chomp  # loeme kasutaja sisendi
  if vastus == 's'
    min = pakkumine + 1 # 6ige vastus on suurem, uus miinimum = pakkumine + 1
  elsif vastus == 'v'
    max = pakkumine - 1 # 6ige vastus on v2iksem, uus maksimum = pakkumine - 1
  end
end

puts 'Tubli!'