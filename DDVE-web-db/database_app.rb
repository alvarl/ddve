require 'sinatra'
require_relative 'config/init'
require_relative 'models/breed'

get '/' do
  erb :breed
end

get '/breed/:id' do
  @breed = Breed[params[:id]]
  erb :breed
end

post '/breed/:id' do
  if params[:id]
    Breed[params[:id]].update(:name => params[:name], :type => params[:type])
  end
  erb :breed
end

post '/breed' do
  if params[:name]
    if params[:id]
      Breed[params[:id]].update(:name => params[:name], :type => params[:type])
    else
      Breed.insert(:name => params[:name], :type => params[:type])
    end
  end

  erb :breed
end
