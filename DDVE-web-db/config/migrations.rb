require 'sinatra/sequel'
require 'amalgalite'

migration "create breed table" do
  database.create_table :breeds do
    primary_key :id
    String :type
    String :name
  end
end

migration "create owner table" do
  database.create_table :owners do
    primary_key :id
    String :name
    Date :birthdate
  end
end

migration "create pet table" do
  database.create_table :pets do
    primary_key :id
    String :name
    Date :birthdate
    foreign_key :owner_id, :owners
    foreign_key :breed_id, :breed
  end
end

migration "insert breeds" do
  dataset = database[:breeds]
  dataset.insert(:type => 'koer', :name => 'dalmaatslane')
  dataset.insert(:type => 'koer', :name => 'saksa lambakoer')
  dataset.insert(:type => 'koer', :name => 'kokkerspanjel')
  dataset.insert(:type => 'koer', :name => 'krants')
  dataset.insert(:type => 'kass', :name => 'siiami')
  dataset.insert(:type => 'kass', :name => 'angoora')
  dataset.insert(:type => 'kass', :name => 'parsia')
end