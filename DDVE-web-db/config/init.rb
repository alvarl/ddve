require 'sinatra/sequel'
require 'amalgalite'

configure :development do
  set :database, 'amalgalite://development.db'
end
configure :test do
  set :database, 'amalgalite://memory:'
end

require_relative 'migrations'
