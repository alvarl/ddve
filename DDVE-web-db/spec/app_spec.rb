require File.expand_path(File.dirname(__FILE__) + '/../app')
require 'rack/test'
require 'rspec'

set :environment, :test

describe 'My Pets app' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "says hello" do
    get '/'
    last_response.should be_ok
    last_response.body.should == 'Hello World!'
  end
end
