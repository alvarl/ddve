require 'sinatra'
require_relative 'config/init'

enable :sessions

get '/' do
  erb :game
end

post '/' do
  if params[:less]
    session[:max] = session[:answer] - 1
  elsif params[:more]
    session[:min] = session[:answer] + 1
  elsif params[:correct]
    @correct = true
  elsif params[:start]
    session[:max] = 100
    session[:min] = 1
    session[:answer] = nil
  end
  @answer = (session[:min] + session[:max]) / 2
  session[:answer] = @answer
  erb :game
end