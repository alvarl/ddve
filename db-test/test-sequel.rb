require 'sequel'

DB = Sequel.amalgalite('my.db')

items = DB[:items]

# print out the number of records
puts "Item count: #{items.count}"

# print out the average price
puts "The average price is: #{items.avg(:price)}"
