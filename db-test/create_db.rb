require 'sequel'

DB = Sequel.amalgalite('my.db')

# create an items table
DB.create_table :items do
  primary_key :id
  String :name
  Float :price
end

DB.create_table :owner do
  primary_key :id
  String :name
  Date :birthdate
end

DB.create_table :breed do
  primary_key :id
  String :type
  String :name
end

DB.create_table :pet do
  primary_key :id
  foreign_key
end


# create a dataset from the items table
items = DB[:items]

# populate the table
items.insert(:name => 'abc', :price => rand * 100)
items.insert(:name => 'def', :price => rand * 100)
items.insert(:name => 'ghi', :price => rand * 100)
